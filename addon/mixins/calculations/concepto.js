import { computed } from '@ember/object';
import { gt, alias, mapBy, sum } from '@ember/object/computed';
import Mixin from '@ember/object/mixin';
import { inject as service } from '@ember/service';

export default Mixin.create({
	precision: service('round-precision'),

	hasDescuento: gt('descuento', 0),


	// Multiplicaci�n de Cantidad * Valour Unitario
	// de acuerdo al SAT
	// Cantidad * ValorUnitario
	importe: computed('cantidad', 'valorUnitario', 'precision.decimals', function () {
		return Decimal(this.get('valorUnitario') || 0).todp(6).mul(this.get('cantidad') || 0).todp(this.get('precision.decimals')).toNumber();
	}).meta({ serialize: true }),


	// Valor del Importe menos el Descuento
	// este deber�a usarse para la base de los impuestos
	// Importe - Descuento
	subtotal: computed('importe', 'descuento', 'precision.decimals', function () {
		return Decimal(this.get('importe') || 0).todp(this.get('precision.decimals')).minus(this.get('descuento') || 0).todp(this.get('precision.decimals')).toNumber();
	}),
	importeConDescuento: alias('subtotal'),


	// La suma de los impuestos
	impuestosImportes: mapBy('impuestos', 'importeForSuma'),
	sumaImpuestos: sum('impuestosImportes'),


	// Importe - Descuento + Impuestos
	// deber�a ocuparse para calcular el Total de la Factura
	total: computed('subtotal', 'sumaImpuestos', function () {
		this.get('impuestos').then((impuestos) => {
			// console.log('concepto#impuestos.length:', impuestos.length);
			impuestos.filterBy('isDeleted', false).invoke('set', 'base', this.get('subtotal'));
		});

		return Decimal(this.get('subtotal')).todp(2).add(this.get('sumaImpuestos')).todp(2).toNumber();
	}),
	subtotalImpuestos: alias('total')

});
