import Mixin from '@ember/object/mixin';
import { not } from '@ember/object/computed';
import {
  validator,
  buildValidations
} from 'ember-cp-validations';
import SaekoFacturaValidations from './saeko/factura';

export default Mixin.create(SaekoFacturaValidations, buildValidations({
  folio: [validator('presence', {
    presence: true,
    message: 'Debe especificar un número de folio',
    disabled: not('model.validateFolio')
  })],
  // serie: [ validator('presence', true) ],
  fecha: [validator('presence', {
    presence: true,
    message: 'Se debe llenar la fecha de cuando se expide la factura'
  })],
  formaPago: [validator('presence', {
    presence: true,
    message: 'Se debe llenar la forma de pago de la factura'
  })],
  subtotal: [validator('presence', true)],
  moneda: [validator('presence', {
    presence: true,
    message: 'Se debe llenar la moneda en la que se expide la factura'
  })],
  // tipoCambio: [ validator('presence', true) ],
  total: [
    validator('presence', true),
    validator('number', {
      positive: true,
      allowString: true,
      message: 'El total de la factura no puede ser negativo'
    })
  ],

  // tipoComprobante: [
  //   validator('presence', true),
  // ],

  metodoPago: [validator('presence', true)],
  lugarExpedicion: [
    validator('presence', {
      presence: true,
      message: 'Se debe llenar el código postal del lugar donde se expide la factura'
    })
  ],
  version: [validator('presence', true)],

  impuestosGlobales: [validator('has-many')],
  conceptos: [
    validator('has-many'),
    validator('length', {
      min: 1,
      message: 'Debe agregar 1 o más conceptos'
    })
  ],
  receptor: [
    validator('belongs-to', {
      disabled: not('model.isNew'),
    }),
    validator('presence', {
      presence: true,
      message: 'Debe elegir o agregar un cliente para la factura',
      disabled: not('model.isNew'),
    })
  ],

  emisor: [
    validator('belongs-to'),
    validator('presence', {
      presence: true,
      message: 'Debe elegir un emisor para la factura'
    })
  ],

  certificado: [
    validator('belongs-to'),
    validator('presence', {
      presence: true,
      message: 'Debe elegir un certificado para la factura'
    })
  ],

  ine: [
    validator('belongs-to')
  ]
}));
