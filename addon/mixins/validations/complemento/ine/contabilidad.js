import {validator,buildValidations} from 'ember-cp-validations';

export default buildValidations({
	idContabilidad: [
		validator('number',{
			allowString: true,
			integer: true,
			positive: true,
			message: 'Las claves de contabilidad INE deben tener solo números'
		}),
		validator('presence', {
			presence: true,
			message: 'Se debe escribir una clave de contabilidad estatal para el proceso INE'
		}),
		validator('length',{
			max: 6,
			message: 'Las claves de contabilidad INE son de máximo 6 números'
		})
	]
});
