import {
	validator
} from 'ember-cp-validations';

export default {
	nombre: validator('presence', true),
	rfc: [
		validator('presence', true),
		validator('format', {
			regex: /^[A-Z&Ñ]{3,4}[0-9]{2}(0[1-9]|1[012])(0[1-9]|[12][0-9]|3[01])[A-Z0-9]{2}[0-9A]$/
		})
	],
	regimen: validator('presence', {
		presence: true,
		message: 'Debe llenar el Regimen Fiscal del emisor de facturas'
	}),
	tipo: [
		validator('presence', {
			presence: true,
			message: 'Debe especificar si es persona Moral o Física el emisor de facturas'
		})
	],
	email: [
		validator('presence', true),
		validator('format', {
			type: 'email'
		})
	],

	fiscalDirecciones: validator('has-many'),
	fiscalCertificados: validator('has-many'),
	fiscalDonataria: validator('belongs-to')
};
