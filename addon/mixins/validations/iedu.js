// import { not } from '@ember/object/computed';
// import Mixin from '@ember/object/mixin';
import { validator, buildValidations } from 'ember-cp-validations';

export default buildValidations({
	curp: [
		validator('presence', {
			presence: true,
			message: 'La CURP del alumno es obligatoria en IEDU'
		}),
		validator('format', {
			regex: /^[A-Z][A,E,I,O,U,X][A-Z]{2}[0-9]{2}[0-1][0-9][0-3][0-9][M,H][A-Z]{2}[B,C,D,F,G,H,J,K,L,M,N,Ñ,P,Q,R,S,T,V,W,X,Y,Z]{3}[0-9,A-Z][0-9]$/,
			message: 'El formato de CURP no es correcto en IEDU'
		})
	],

	nombreAlumno: [
		validator('presence', {
			presence: true,
			message: 'El nombre del alumno es obligatorio en IEDU'
		}),
	],

	nivelEducativo: [
		validator('presence', {
			presence: true,
			message: 'El nivel educativo es obligatorio en IEDU'
		}),
	],
	autRVOE: [
		validator('presence', {
			presence: true,
			message: 'El autRVOE es obligatorio en IEDU'
		}),
	],
});
