import { inject as service } from '@ember/service';
// import { camelize } from 'ember-string';
// import { pluralize } from 'ember-inflector';
import FirebaseAdapter from 'emberfire/adapters/firebase';
// import FirebaseFlexAdapter from 'emberfire-utils/adapters/firebase-flex';
import CascadeDeleteMixin from 'ember-data-cascade-delete';
import { makeArray } from '@ember/array';

export default FirebaseAdapter.extend(CascadeDeleteMixin, {
	currentAccount: service(),

	recordWillDelete: function recordWillDelete(/*store, record*/) {
		// here we are doing nothing. Emberfire try to update parent for belongTo relationship directly
		// on firebase, which would lead sometimes to an error as the child get updated, while we delete it
		// when the parent receive the loopback from firebase.
	},

	pathForType(modelName){
		let path = this._super(...arguments);

		if(modelName == 'user'){
			return path;
		}

		let accountId = this.get('currentAccount.id');
		return `accounts/${accountId}/${path}`;
	},


	_applyRangesToRef(ref, query) {
    const methods = ['equalTo', 'startAt', 'endAt'];
    methods.forEach(key => {
      if (query[key] !== undefined) {
        ref = ref[key](...makeArray(query[key]));
      }
    });


    return ref;
  }

	// query(store, type, query = {}, recordArray){
	// 	let collection = camelize(pluralize(type.modelName));

	// 	query.path = `/users/${this.get('session.currentAccount.uid')}/${collection}`;
	// 	query.isReference = true;

	// 	return this._super(store, type, query, recordArray);
	// }
});
s