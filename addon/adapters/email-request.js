import FirebaseAdapter from 'emberfire/adapters/firebase';
import CascadeDeleteMixin from 'ember-data-cascade-delete';

export default FirebaseAdapter.extend(CascadeDeleteMixin, {
	recordWillDelete: function recordWillDelete(/*store, record*/) {
		// here we are doing nothing. Emberfire try to update parent for belongTo relationship directly
		// on firebase, which would lead sometimes to an error as the child get updated, while we delete it
		// when the parent receive the loopback from firebase.
	}
});
