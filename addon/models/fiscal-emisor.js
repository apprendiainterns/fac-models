import { alias, filterBy, notEmpty, readOnly } from '@ember/object/computed';
import { computed } from '@ember/object';
import { all } from 'rsvp';
import DS from 'ember-data';
import {
	buildValidations
} from 'ember-cp-validations';
import ValidationDefinitions from 'ember-cli-facturacion-logic/mixins/validations/fiscal-emisor';
import PersonaDefinitions from 'ember-cli-facturacion-logic/mixins/calculations/fiscal-persona';

const Validations = buildValidations(ValidationDefinitions);

export default DS.Model.extend(Validations, PersonaDefinitions,{
	nombre: DS.attr("string"),
	rfc: DS.attr("string"),
	regimen: DS.attr("string"),
	tipo: DS.attr("number"),
	email: DS.attr("string", { defaultValue: '' }),
	nombreComercial: DS.attr("string"),

	// Complementos
	autRVOE: DS.attr('string'), // It is used as a default for conceptos

	/*users: DS.belongsTo("user"),
	logotipo: DS.belongsTo("archivo",{polymorphic:true}),*/
	templates: DS.hasMany('facturaTemplate'),
	fiscalDirecciones: DS.hasMany("fiscalDireccion"),
	direcciones: alias('fiscalDirecciones'),

	fiscalDonataria: DS.belongsTo("fiscalDonataria"),
	donataria: alias('fiscalDonataria'),

	fiscalCertificados: DS.hasMany("fiscalCertificado", {
		cascadeDelete: true
	}),
	certificados: alias('fiscalCertificados'),

	validCertificados: filterBy('fiscalCertificados', 'isUsable', true),
	hasValidCertificados: notEmpty('validCertificados'),


	facturas: computed(function(){
		return DS.PromiseArray.create({
			promise: this.get('store').query('factura', {
				orderBy: 'emisor',
				equalTo: this.get('id')
			})
		})
	}).volatile(),


	fullSave() {
		let promises = [this.save()];

		if (this.hasMany('fiscalDirecciones').value()) {
			promises.pushObject(all(this.hasMany('fiscalDirecciones').value().invoke('save')));
		}

		if (this.hasMany('fiscalCertificados').value()) {
			promises.pushObject(all(this.hasMany('fiscalCertificados').value().invoke('save')));
		}

		if (this.hasMany('templates').value()) {
			promises.pushObject(all(this.hasMany('templates').value().invoke('save')));
		}

		return all(promises);
	},
});
