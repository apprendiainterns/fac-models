
import DS from 'ember-data';
import {all} from 'rsvp';
import {filterBy} from '@ember/object/computed';
import Copyable from 'ember-data-copyable';
import Validations from 'ember-cli-facturacion-logic/mixins/validations/complemento/ine/entidad';

export default DS.Model.extend(Validations, Copyable, {
  copyableOptions: {},

  claveEntidad: DS.attr('string'),
  ambito: DS.attr('string'),

  contabilidades: DS.hasMany('complemento/ine/contabilidad', { cascadeDelete: true }),
  contabilidadesLive: filterBy('contabilidades', 'isDeleted', false),

  fullSave() {
    let promises = [this.save()];

    promises.pushObject(
      this.get('contabilidades').then(function(contabilidades){
        debugger
        return all(contabilidades.invoke('save'))
      })
    )

    return all(promises);
  },
});
