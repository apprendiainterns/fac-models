import { filterBy } from '@ember/object/computed';
import { inject as service } from '@ember/service';
import { isBlank } from '@ember/utils';
import { computed, get } from '@ember/object';
import DS from 'ember-data';
import Copyable from 'ember-data-copyable';
import Calculations from 'ember-cli-facturacion-logic/mixins/calculations/factura';
import Emailing from 'ember-cli-facturacion-logic/mixins/facturas/actions/emailing';
import moment from 'moment';
import Validations from 'ember-cli-facturacion-logic/mixins/validations/factura';

export default DS.Model.extend(Copyable, Calculations, Validations, Emailing, {
  currentUser: service(),

  webhookUrl: DS.attr("string"),
  successWebhookUrl: DS.attr("string"),
  cancelWebhookUrl: DS.attr("string"),

  templateId: DS.attr('string'),
  templateCode: DS.attr('string'),
  decimals: DS.attr('number', { defaultValue: '2' }), //necesario para los default de decimales
  coinDecimals: DS.attr('boolean', { defaultValue: true }),
  prueba: DS.attr("boolean", { default: false }),
  folio: DS.attr("string"),
  uuid: DS.attr("string"),
  serie: DS.attr("string"),
  fecha: DS.attr("string"),
  fechaUnix: computed('fecha', function () {
    return (!isBlank(this.get('fecha'))) ? moment.utc(this.get('fecha')).unix() : 0;
  }).meta({ serialize: true }),
  fechaTimbrado: DS.attr("string"),
  fechaCancelado: DS.attr("string"),
  formaPago: DS.attr({ defaultValue() { return [] } }),
  condicionesPago: DS.attr("string"),
  moneda: DS.attr("string", {
    defaultValue: 'MXN'
  }),
  tipoCambio: DS.attr("number"),
  metodoPago: DS.attr("string", { defaultValue: 'PUE' }),
  lugarExpedicion: DS.attr("string"),
  confirmacion: DS.attr("string"),
  tipoComprobante: DS.attr("string", { defaultValue: 'I' }),
  version: DS.attr("number", { defaultValue: '3.3' }),
  usoCfdi: DS.attr("string"),
  recarga: DS.belongsTo('recarga'),

  // subtotal: DS.attr("number"),
  // total: DS.attr("number"),
  // descuento: DS.attr("number"),

  emisor: DS.belongsTo("fiscalEmisor"),
  certificado: DS.belongsTo("fiscalCertificado"),
  receptor: DS.belongsTo("fiscalReceptor"),
  conceptos: DS.hasMany("concepto"),
  cfdiRelacionado: DS.belongsTo("cfdiRelacionado"),
  impuestosGlobales: DS.hasMany("impuesto"),
  timbrarRequest: DS.belongsTo("timbrarRequest"),
  cancelarRequest: DS.belongsTo("cancelarRequest"),
  emailRequests: DS.hasMany('emailRequest'),

  // Complementos
  ine: DS.belongsTo('complemento/ine'),


  pdfUrl: DS.attr("string"),
  xmlUrl: DS.attr("string"),
  xmlCanceladoUrl: DS.attr("string"),

  globalesFederales: filterBy('impuestosGlobales', 'isLocal', false),
  globalesLocales: filterBy('impuestosGlobales', 'isLocal'),

  editable: computed('prueba', 'uuid', function () {
    return this.get('prueba') || isBlank(this.get('uuid'));
  }),

  isCancelada: computed.or('fechaCancelado', 'xmlCanceladoUrl'),


  conceptosJoined: computed('conceptos.@each.descripcion', function(){
    return this.get('conceptos').mapBy('descripcion').join(', ');
  }),

  template: computed('templateCode', 'templateId', {
    get() {
      if(!this.get('templateCode')){ return null }
      return DS.PromiseArray.create({
        promise: this.get('emisor.templates')
          .then((arr) => {
            return arr.findBy('code', this.get('templateCode'));
          })
      })
    },
    set(_, template) {
      if (template) {
        this.set('templateCode', get(template, 'code'));
      }
      return template;
    }
  }),

  resetTimbrable(attrs) {
    this.setProperties(Object.assign({
      uuid: null,
      pdfUrl: null,
      xmlUrl: null,
      fechaTimbrado: null
    }, attrs || {}))
  },

  timbrar: async function (attrs) {
    let timbrarRequest = await this.get('timbrarRequest');
    if (timbrarRequest) {
      timbrarRequest.setProperties({ status: null, error: null });
    } else {
      let accountId = this.get('currentUser.model.account') || this.get('currentUser.account')

      if(!accountId){
        return alert('A ocurrido un problema innesperado, por favor contacte a soporte y proporcione el código: NoAcc01')
      }

      timbrarRequest = this.set('timbrarRequest', this.store.createRecord('timbrarRequest', {
        factura: this,
        account: accountId
      }));
    }

    return new Promise((resolve, reject) => {
      let onStatusChanged = function () {
        switch (this.get('status')) {
          case 2:
            resolve();
            break;
          case 3:
            reject(this.get('error'));
            break;
        }
        try{
          this.removeObserver('status', this, onStatusChanged);
        }catch(e){ console.log(e); }
      };

      timbrarRequest.addObserver('status', timbrarRequest, onStatusChanged);
      timbrarRequest.save();
    })
  }
});
