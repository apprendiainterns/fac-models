import DS from 'ember-data';
import { computed } from '@ember/object';

export default DS.Model.extend({
	folio: DS.attr('string'),
	fechaUnix: DS.attr('number'),
	receptorNombre: DS.attr('string'),
	receptorRfc: DS.attr('string'),
	uuid: DS.attr('string'),
	total: DS.attr('number'),

	prueba: DS.attr('boolean'),
	isCancelada: DS.attr('boolean'),
	editable: DS.attr('boolean'),

	label: computed('folio', 'receptorNombre', 'receptorRfc', 'total', function(){
		return `${this.get('folio')} ${this.get('receptorNombre')} ${this.get('receptorRfc')} ${this.get('total')}`
	}),

	factura: computed('id', function(){
		return DS.PromiseObject.create({
			promise: this.store.find('factura', this.get('id'))
		})
	})
});
