import DS from 'ember-data';

export default DS.Model.extend({
	account: DS.attr('string'),
	factura: DS.belongsTo('factura'),
	to: DS.attr({defaultValue(){ return [] }})
});
