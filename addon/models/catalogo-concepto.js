import Concepto from './concepto';

export default Concepto.extend({
	validateCantidad: false,
	validateImporte: false
});
