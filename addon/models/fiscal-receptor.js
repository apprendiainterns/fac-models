import { computed } from '@ember/object';
import { all } from 'rsvp';
import { alias } from '@ember/object/computed';
import DS from 'ember-data';
import {
  validator,
  buildValidations
} from 'ember-cp-validations';
import PersonaDefinitions from 'ember-cli-facturacion-logic/mixins/calculations/fiscal-persona';

const Validations = buildValidations({
  nombre: validator('presence', true),

  rfc: [
    validator('presence', true),
    validator('format', {
      regex: /^[A-Z&Ñ]{3,4}[0-9]{2}(0[1-9]|1[012])(0[1-9]|[12][0-9]|3[01])[A-Z0-9]{2}[0-9A]$/
    })
  ],

  email: [
    validator('format', { type: 'email', allowBlank: true })
  ],

  // fiscalDirecciones: validator('has-many')
});

export default DS.Model.extend(Validations, PersonaDefinitions, {
  nombre: DS.attr("string"),
  rfc: DS.attr("string"),
  email: DS.attr("string", {
    defaultValue: ''
  }),
  manuallyCreated: DS.attr("boolean"),

  fiscalDirecciones: DS.hasMany("fiscalDireccion"),
  direcciones: alias('fiscalDirecciones'),
  ine: DS.belongsTo('complemento/ine'),
  // facturas: DS.hasMany('factura'),

  fullDisplayName: computed('nombre', 'rfc', function() {
    return `${this.get('rfc') || ''} - ${this.get('nombre') || ''}`;
  }),

  fullSave() {
    let promises = [this.save()];

    if (this.hasMany('fiscalDirecciones').value()) {
      promises.pushObject(all(this.hasMany('fiscalDirecciones').value().invoke('save')));
    }

    if(this.belongsTo('ine').value()){
      promises.pushObject(this.belongsTo('ine').value().fullSave());
    }

    return all(promises);
  },

  searchableField: computed(function(){
    return `${this.get('rfc')} ${this.get('nombre')}`;
  })
});
