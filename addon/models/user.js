import DS from 'ember-data';
import {computed} from '@ember/object';

export default DS.Model.extend({
	access_token: DS.attr('string'),
	account: DS.attr('string'),

	saeko: DS.attr('boolean'),

	firstTimeEnter: true,

	isSaeko: computed('saeko', function(){
		return this.get('saeko') === true;
	})
});
