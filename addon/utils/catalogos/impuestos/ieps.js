const id = '003';
const obj = {
  id: id,
  label: 'IEPS',
  inputTypes: ['range', 'fixed'],
  fixedValues: [
    '0.265000',
    '0.300000',
    '0.530000',
    '0.500000',
    '1.600000',
    '0.304000',
    '0.250000',
    '0.090000',
    '0.080000',
    '0.070000',
    '0.060000',
    '0.030000',
    '0.000000'
  ],
  range: {
    min: 0.000000,
    max: 43.770000
  },
  trasladado: true,
  retencion: true
}
export {id as default, obj}
