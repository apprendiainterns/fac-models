const id = '002';
const obj = {
  id: id,
  label: 'IVA',
  inputTypes: ['range', 'fixed'],
  fixedValues: [
    '0.160000',
    '0.000000'
  ],
  range: {
    min: 0,
    max: 0.160000
  },
  trasladado: true,
  retencion: true
};
export {
  id as
  default, obj
}
