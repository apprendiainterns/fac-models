import { set } from '@ember/object';
import { copy } from '@ember/object/internals';
import EmberObject from '@ember/object';

export const TRASLADADO = 2;
export const RETENCION = 1;
export const FACTOR_CUOTA = 'Cuota';
export const FACTOR_TASA = 'Tasa';

import C_IMPUESTO_ISR, { obj as ISR } from './impuestos/isr';
export { C_IMPUESTO_ISR };

import C_IMPUESTO_IVA, { obj as IVA } from './impuestos/iva';
export { C_IMPUESTO_IVA };

import C_IMPUESTO_IEPS, { obj as IEPS } from './impuestos/ieps';
export { C_IMPUESTO_IEPS};

import C_IMPUESTO_LOCAL, { obj as LOCAL } from './impuestos/locales';
export { C_IMPUESTO_LOCAL };

let impuestos = {}
impuestos[`${C_IMPUESTO_ISR}`] = ISR;
impuestos[`${C_IMPUESTO_IVA}`] = IVA;
impuestos[`${C_IMPUESTO_IEPS}`] = IEPS;
impuestos[`${C_IMPUESTO_LOCAL}`] = LOCAL;
export default impuestos;


const impuestosArray = Object.values(impuestos);
const groupMapFunction = (tipo) => {
  return (object) => {
    let clone = copy(object, true);
    set(clone, 'tipo', tipo);
    return EmberObject.create(clone);
  };
};


export let grouped = [
  {
    groupName: 'Impuestos trasladados',
    options: impuestosArray.filterBy('trasladado').map(groupMapFunction(TRASLADADO)),
    tipo: TRASLADADO
  },
  {
    groupName: 'Impuestos retenidos',
    options: impuestosArray.filterBy('retencion').map(groupMapFunction(RETENCION)),
    tipo: RETENCION
  }
];
