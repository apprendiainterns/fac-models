import { helper } from '@ember/component/helper';
import INEConstants from 'facturacion/utils/catalogos/complemento-ine';

export function disableAmbitoByEntidad(params/*, hash*/) {
  let claveEntidad = params[0], option = params[1];
  return option == INEConstants.AMBLOCAL && Object.keys(INEConstants.NOTENTIDADES).contains(claveEntidad) ? true : null
}

export default helper(disableAmbitoByEntidad);
