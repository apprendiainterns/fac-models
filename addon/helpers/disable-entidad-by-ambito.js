import { helper } from '@ember/component/helper';
import INEConstants from 'facturacion/utils/catalogos/complemento-ine';

export function disableEntidadByAmbito(params/*, hash*/) {
	let ambito = params[0], key = params[1];
  return ambito == INEConstants.AMBLOCAL && Object.keys(INEConstants.NOTENTIDADES).contains(key) ? true : null
}

export default helper(disableEntidadByAmbito);
