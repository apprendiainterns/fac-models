import { helper } from '@ember/component/helper';

export function percent(params/*, hash*/) {
  return `${Number(params[0]) * 100}`;
}

export default helper(percent);
