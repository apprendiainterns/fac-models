import { inject as service } from "@ember/service";
import { get } from "@ember/object";

import FirebaseSerializer from 'emberfire/serializers/firebase';
// import FirebaseFlexSerializer from 'emberfire-utils/serializers/firebase-flex';
import DS from 'ember-data';

export default FirebaseSerializer.extend(DS.EmbeddedRecordsMixin, {
	session: service(),

	serialize(snapshot /*, options*/ ) {
		let json = this._super(...arguments);

		// const path = this._getPath(snapshot);

		snapshot.record.constructor.eachComputedProperty((key, meta) => {
			if (get(meta, 'serialize')) {
				let prop = snapshot.record.get(key);
				if (prop && prop.then) {
					prop = get(prop, 'content');
				}
				json[`${key}`] = prop;
				// json[`${path}/${key}`] = prop;
			}
		});

		// json['user'] = this.get('session.currentUser.uid');

		return json;
	}
});
