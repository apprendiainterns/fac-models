import EmberObject from '@ember/object';
import ValidationsComplementoIneMixin from 'ember-cli-facturacion-logic/mixins/validations/complemento/ine';
import { module, test } from 'qunit';

module('Unit | Mixin | validations/complemento/ine', function() {
  // Replace this with your real tests.
  test('it works', function (assert) {
    let ValidationsComplementoIneObject = EmberObject.extend(ValidationsComplementoIneMixin);
    let subject = ValidationsComplementoIneObject.create();
    assert.ok(subject);
  });
});
