import EmberObject from '@ember/object';
import ValidationsImpuestoMixin from 'ember-cli-facturacion-logic/mixins/validations/impuesto';
import { module, test } from 'qunit';

module('Unit | Mixin | validations/impuesto', function() {
  // Replace this with your real tests.
  test('it works', function (assert) {
    let ValidationsImpuestoObject = EmberObject.extend(ValidationsImpuestoMixin);
    let subject = ValidationsImpuestoObject.create();
    assert.ok(subject);
  });
});
