import EmberObject from '@ember/object';
import FacturasActionsEmailingMixin from 'ember-cli-facturacion-logic/mixins/facturas/actions/emailing';
import { module, test } from 'qunit';

module('Unit | Mixin | facturas/actions/emailing', function() {
  // Replace this with your real tests.
  test('it works', function (assert) {
    let FacturasActionsEmailingObject = EmberObject.extend(FacturasActionsEmailingMixin);
    let subject = FacturasActionsEmailingObject.create();
    assert.ok(subject);
  });
});
