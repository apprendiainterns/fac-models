import EmberObject from '@ember/object';
import FacturasHandlersImpuestosGlobalesMixin from 'ember-cli-facturacion-logic/mixins/facturas/handlers/impuestos-globales';
import { module, test } from 'qunit';

module('Unit | Mixin | facturas/handlers/impuestos-globales', function() {
  // Replace this with your real tests.
  test('it works', function (assert) {
    let FacturasHandlersImpuestosGlobalesObject = EmberObject.extend(FacturasHandlersImpuestosGlobalesMixin);
    let subject = FacturasHandlersImpuestosGlobalesObject.create();
    assert.ok(subject);
  });
});
