import EmberObject from '@ember/object';
import ConfigEnviromentMixin from 'ember-cli-facturacion-logic/mixins/config/enviroment';
import { module, test } from 'qunit';

module('Unit | Mixin | config/enviroment', function() {
  // Replace this with your real tests.
  test('it works', function (assert) {
    let ConfigEnviromentObject = EmberObject.extend(ConfigEnviromentMixin);
    let subject = ConfigEnviromentObject.create();
    assert.ok(subject);
  });
});
