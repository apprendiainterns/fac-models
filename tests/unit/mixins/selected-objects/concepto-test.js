import EmberObject from '@ember/object';
import SelectedObjectsConceptoMixin from 'ember-cli-facturacion-logic/mixins/selected-objects/concepto';
import { module, test } from 'qunit';

module('Unit | Mixin | selected-objects/concepto', function() {
  // Replace this with your real tests.
  test('it works', function (assert) {
    let SelectedObjectsConceptoObject = EmberObject.extend(SelectedObjectsConceptoMixin);
    let subject = SelectedObjectsConceptoObject.create();
    assert.ok(subject);
  });
});
